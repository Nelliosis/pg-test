const db = require('./model.js');
const pg = require("./data/dbConfig.js");


console.log("these tests assume the database is named \"pokemon\" with table \"cards\" and 3 initial seed entries.")


const May = {
    name: "May",
    description: {
        title: "The second Girl"
    }
}

const Selena = {
    name: "Selena",
    description: {
        title: "The best gym leader"
    }
}

const message = {
    message: "User not found"
}

//retrieveAll() tests. Last part is at the end.
test("retrieveAll() successfully retrieves initial seed items", async () => {
    const cards = "cards";
    const data = await db.retrieveAll(cards);
    expect(data.length).toEqual(3)
})
test("retrieveAll() correctly identifies if table is not cards", async () => {
    const test = "test";
    const data = await db.retrieveAll(test);
    const message = { message: "critical error" };
    expect(data).toEqual(message);
})


//insert() tests
test("insert() successfully inserts a new entry", async () => {
    const data = await db.insert(Selena.name, Selena.description);
    expect(data[0]).toEqual(expect.objectContaining({ "id": expect.any(Number) }))
})
test("insert() successfully returns an error when name is of invalid type", async () => {
    const message = { message: "invalid name" };
    const data = await db.insert(345, Selena.description);
    expect(data).toEqual(expect.objectContaining(message))
})
test("insert() successfully returns an error when description is of invalid type", async () => {
    const message = { message: "invalid description" };
    const data = await db.insert(Selena.name, 123);
    expect(data).toEqual(expect.objectContaining(message))
})


//retrieve() tests
test("retrieve() successfully retrieves by ID that exists", async () => {
    const data = await db.retrieve(2);

    expect(data).toEqual(May);
})
test("retrieve() successfully returns a UserNotFound message if id doesn't exist", async () => {
    const data = await db.retrieve(200);

    expect(data).toEqual(message);
})
test("retrieve() successfully returns a InvalidID message if id is not an integer", async () => {
    const data = await db.retrieve("asd");
    const message = {
        message: "invalid id"
    }
    expect(data).toEqual(message);
})

//update() tests
test("update() successfully retrieves id and changes name & description", async () => {
    const description = { title: "One of the best" };
    const data = await db.update("Gloria", 4, description);
    const message = { message: "update success" };
    expect(data).toEqual(message);
})
test("update() successfully returns an error message when id of wrong type", async () => {
    const description = { title: "One of the best" };
    const data = await db.update("test", "owo", description);
    const message = { message: "invalid id" };
    expect(data).toEqual(message);
})
test("update() successfully returns an error message when user doesn't exist", async () => {
    const description = { title: "One of the best" };
    const data = await db.update("Gloria", 100, description);
    const message = { message: "User not found" };
    expect(data).toEqual(message);
})

//deleteCard() tests
test("deleteCard() successfully deletes a user", async () => {
    const data = await db.deleteCard(1);
    const message = { message: "Delete success" }
    expect(data).toEqual(message);
})
test("deleteCard() successfully returns an error if id is not type int", async () => {
    const data = await db.deleteCard("test");
    const message = { message: "invalid id" }
    expect(data).toEqual(message);
})
test("deleteCard() successfully returns an error if id is less than 0", async () => {
    const data = await db.deleteCard(0);
    const message = { message: "invalid id" }
    expect(data).toEqual(message);
})

//final retrieveAll test
test("retrieveAll() successfully returns a message if the database has a critical error", async () => {
    const cards = "cards";
    await pg.schema.dropTableIfExists('cards');
    const message = { message: "critical error" }

    const result = await db.retrieveAll(cards);
    expect(result).toEqual(message)
}) 
