const pg = require("./data/dbConfig.js");
const fs = require('fs');

const pubKey = fs.readFileSync('./keys/public.key').toString();
const secKey = fs.readFileSync('./keys/private.key').toString();



async function insert(name, description) {

    if (typeof (name) != typeof ("")) return { message: "invalid name" };
    if (typeof (description) != typeof ({})) return { message: "invalid description" }

    return pg("cards").insert(pg.raw(`(id, name, description) VALUES (DEFAULT, pgp_pub_encrypt(?, dearmor(?)), ?)`, [name, pubKey, { title: description.title }])).returning('id');

    // return pg.rows(
    //     `INSERT INTO cards(name) VALUES (pgp_pub_encrypt($1, dearmor(pubkey))) RETURNING id`, data
    // )
}

async function retrieveAll(table) {

    return pg(`${table}`).select(pg.raw(`id, pgp_pub_decrypt(name, dearmor(?)) AS name, description`, secKey))
        .then(users => {
            if (users[0] === undefined) return { message: "Database empty" };
            return users;
        }).catch(err => {
            return { message: "critical error" };
        })

    // return pg.rows(
    //     `SELECT id, pgp_pub_decrypt(name, dearmor(${seckey})) AS name FROM cards`,
    // )
}

async function retrieve(id) {

    if (typeof (id) === typeof ("")) return { message: "invalid id" };

    return pg.select(pg.raw(`pgp_pub_decrypt(name, dearmor(?)) AS name, description FROM cards WHERE id = ?`, [secKey, id]))
        .then(user => {
            const result = user[0];
            if (result === undefined) {
                return { message: "User not found" };
            }
            return result;
        })

    // return pg.select('name', 'description').from('cards').where('id', id)

    // return pg.rows(
    //     `SELECT pgp_pub_decrypt(name, dearmor(${seckey})) AS name FROM cards WHERE id = $1`, id
    // )
}

async function update(name, id, description) {

    if (typeof (id) === typeof ("")) return { message: "invalid id" };

    const idCheck = await pg.select('id').from('cards').where('id', id);
    if (idCheck[0] === undefined) return { message: "User not found" };

    return pg.raw(`UPDATE cards SET name = pgp_pub_encrypt(?, dearmor(?)), description = ? WHERE id = ?`, [name, pubKey, { title: description.title }, id])
        .then(() => {
            return { message: "update success" };
        });

    //return pg.update({ name: data, description: { title: description.title } }).from('cards').where('id', id);

    // return pg.rows(
    //     `UPDATE cards SET name = pgp_pub_encrypt($1, dearmor(${pubkey})) WHERE id = $2`, data, id
    // )
}

async function deleteCard(id) {

    if (typeof (id) === typeof ("") || id <= 0) return { message: "invalid id" };

    return pg('cards').where('id', id).del()
        .then(() => {
            return { message: "Delete success" };
        });
}

module.exports = {
    insert, //done
    retrieveAll, //done
    retrieve, //done
    update, // done
    deleteCard, //done
}
