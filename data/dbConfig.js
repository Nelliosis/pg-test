const knex = require('knex');
const knexfile = require('../knexfile');

const env = 'development';
const dbConfig = knexfile[env];

module.exports = knex(dbConfig);
