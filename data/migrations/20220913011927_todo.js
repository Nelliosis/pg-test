/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = knex =>
    knex.schema.raw('CREATE EXTENSION IF NOT EXISTS pgcrypto')
        .createTable("cards", tbl => {
            tbl.increments("id");
            tbl.binary("name");
            tbl.jsonb("description");
        });

//     raw(`CREATE TABLE cards(
//         id SERIAL PRIMARY KEY,
//         name BYTEA,
//         description JSONB
//     )`).then(() => {
//     knex
// })




/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = knex => knex.schema.dropTableIfExists("cards");
